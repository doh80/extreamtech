package com.extreamtech.daniel.extreamtech.engine;

import android.content.Context;

/**
 * Created by Daniel on 2016-09-13.
 */
public class Engine {

    private static Context mContext;
    private static Engine mEngine;

    private static VolleyManager mVolleyManager;

    public static Engine getInstance(Context context) {
        if (mEngine == null) {
            mEngine = new Engine(context);
        }

        return mEngine;
    }


    private Engine(Context context) {
        mContext = context;
        startUp();

    }

    public static void startUp() {
        mVolleyManager = VolleyManager.getInstance(mContext);

    }

    public static VolleyManager getVolleyManager() {
        if (mVolleyManager == null) {
            mVolleyManager = VolleyManager.getInstance(mContext);
        }

        return mVolleyManager;
    }


}
