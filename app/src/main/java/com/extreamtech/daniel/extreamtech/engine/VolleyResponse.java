package com.extreamtech.daniel.extreamtech.engine;

import com.android.volley.Response;

/**
 * Created by Daniel on 2016-09-13.
 */
public class VolleyResponse<T> {

    public Response.Listener<T> responseListener = null;
    public Response.ErrorListener errorListener = null;

    public VolleyResponse(Response.Listener<T> responseListener, Response.ErrorListener errorListener) {
        this.responseListener = responseListener;
        this.errorListener = errorListener;

    }

}
