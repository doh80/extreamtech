package com.extreamtech.daniel.extreamtech.main_screen.models;

import java.io.Serializable;

/**
 * Created by Daniel on 2016-09-16.
 */
public class Dashboard implements Serializable{

    private String title;
    private String subtitle;
    private String subtitle2;

    public Dashboard() {

    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getSubtitle2() {
        return subtitle2;
    }

}
