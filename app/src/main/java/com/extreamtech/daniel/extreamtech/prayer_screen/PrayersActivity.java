package com.extreamtech.daniel.extreamtech.prayer_screen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.extreamtech.daniel.extreamtech.BaseActivity;
import com.extreamtech.daniel.extreamtech.R;
import com.extreamtech.daniel.extreamtech.Utils.DateFormatUtils;
import com.extreamtech.daniel.extreamtech.engine.Engine;
import com.extreamtech.daniel.extreamtech.engine.VolleyManager;
import com.extreamtech.daniel.extreamtech.engine.VolleyResponse;
import com.extreamtech.daniel.extreamtech.prayer_screen.models.Prayer;
import com.extreamtech.daniel.extreamtech.uicomponent.MontserratRegularTextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;


public class PrayersActivity extends BaseActivity {

    public static final String CHURCH_ID_KEY = "CHURCH_ID_KEY";

    private Toolbar prayerToolBarView;
    private RecyclerView prayerRecyclerView;
    private PrayerListViewAdapter prayerListViewAdapter;
    private ArrayList<Prayer> prayersList;

    private SwipeRefreshLayout prayerRefreshLayout;

    private Context context;

    private int churchId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prayers);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                if (bundle.containsKey(CHURCH_ID_KEY)) {
                    this.churchId = bundle.getInt(CHURCH_ID_KEY);
                }
            }
        }

        this.context = this;
        this.prayersList = new ArrayList<>();

        initUI();
        fetchPrayerData(urlBuilder(churchId));

    }


    private String urlBuilder(int churchID) {
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.prayers_url));
        sb.append(churchID);
        sb.append(getString(R.string.prayers_url_end));

        return sb.toString();
    }


    private void fetchPrayerData(String url) {

        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Type collectionType = new TypeToken<Collection<Prayer>>(){}.getType();
                prayersList.clear();
                prayersList = new Gson().fromJson(response, collectionType);

                prayerListViewAdapter.setPrayerList(prayersList);
                prayerListViewAdapter.notifyDataSetChanged();

                prayerRefreshLayout.setRefreshing(false);

            }
        };

        Response.ErrorListener ResponseErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                prayerRefreshLayout.setRefreshing(false);

            }
        };

        VolleyManager volleyManager = Engine.getInstance(this).getVolleyManager();
        volleyManager.getStringRequest(Request.Method.GET, url, new VolleyResponse<>(responseListener, ResponseErrorListener), null);

    }



    private void initUI() {

        //Toolbar
        prayerToolBarView = (Toolbar) findViewById(R.id.prayerToolBarView);
        prayerToolBarView.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        prayerToolBarView.setTitle(getString(R.string.prayers));
        setSupportActionBar(prayerToolBarView);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        prayerToolBarView.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });


        prayerRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.prayer_refresh_layout);
        prayerRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchPrayerData(urlBuilder(churchId));

            }
        });

        prayerToolBarView = (Toolbar) findViewById(R.id.prayerToolBarView);
        prayerRecyclerView = (RecyclerView) findViewById(R.id.prayerRecyclerView);
        prayerListViewAdapter = new PrayerListViewAdapter(context, prayersList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        prayerRecyclerView.setLayoutManager(mLayoutManager);
        prayerRecyclerView.setAdapter(prayerListViewAdapter);

    }


    private class PrayerListViewAdapter extends RecyclerView.Adapter<PrayerViewHolders> {

        private ArrayList<Prayer> itemList;
        private Context mContext;

        public PrayerListViewAdapter(Context context, ArrayList<Prayer> itemList) {
            this.itemList = itemList;
            this.mContext = context;
        }


        public void setPrayerList(ArrayList<Prayer> itemList) {
            this.itemList = itemList;
        }


        @Override
        public PrayerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(mContext).inflate(R.layout.prayer_list_view, parent, false);
            PrayerViewHolders rcv = new PrayerViewHolders(layoutView);
            return rcv;

        }


        @Override
        public void onBindViewHolder(PrayerViewHolders holder, final int position) {
            holder.nameTextView.setText(itemList.get(position).getName());
//            holder.date.setText(itemList.get(position).getDate());

            long milliseconds = DateFormatUtils.convertFormattedDateToMilliseconds(itemList.get(position).getDate());
            holder.date.setText(DateFormatUtils.getTimeAgo(milliseconds));
            holder.messageTextView.setText(itemList.get(position).getMessage());
            holder.numOfPrayerTextView.setText("" + itemList.get(position).getNumberOfPrayers() + " " + getString(R.string.prayers));

            if ((position % 2) == 0) {
                holder.prayerListLayout.setBackgroundColor(getColor(R.color.primary_text));

            } else {
                holder.prayerListLayout.setBackgroundColor(getColor(R.color.divider_gray));

            }

        }

        @Override
        public int getItemCount() {
            return this.itemList.size();
        }

    }


    public class PrayerViewHolders extends RecyclerView.ViewHolder {

        public MontserratRegularTextView nameTextView;
        public MontserratRegularTextView date;
        public MontserratRegularTextView messageTextView;
        public MontserratRegularTextView numOfPrayerTextView;

        public RelativeLayout prayForPerson;
        public RelativeLayout prayerListLayout;

        public PrayerViewHolders(View itemView) {
            super(itemView);

            nameTextView = (MontserratRegularTextView) itemView.findViewById(R.id.nameTextView);
            date = (MontserratRegularTextView) itemView.findViewById(R.id.date);
            messageTextView = (MontserratRegularTextView) itemView.findViewById(R.id.messageTextView);
            numOfPrayerTextView = (MontserratRegularTextView) itemView.findViewById(R.id.numOfPrayerTextView);

            prayerListLayout = (RelativeLayout) itemView.findViewById(R.id.prayerListLayout);
            prayForPerson = (RelativeLayout) itemView.findViewById(R.id.prayForPerson);

        }

    }


}
