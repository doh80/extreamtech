package com.extreamtech.daniel.extreamtech.main_screen;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.extreamtech.daniel.extreamtech.R;
import com.extreamtech.daniel.extreamtech.main_screen.models.Dashboard;
import com.extreamtech.daniel.extreamtech.uicomponent.MontserratBoldTextView;
import com.extreamtech.daniel.extreamtech.uicomponent.MontserratRegularTextView;


public class MainScreenSlidePageFragment extends Fragment {

    private static final String DASHBOARD_KEY = "DASHBOARD_KEY";

    private MontserratBoldTextView titleTextView;
    private MontserratRegularTextView subtitleTextView;
    private MontserratRegularTextView subtitle2TextView;

    private Dashboard mDashboard;

    public MainScreenSlidePageFragment() {}

    public static MainScreenSlidePageFragment newInstance(Dashboard dashboard) {
        MainScreenSlidePageFragment fragment = new MainScreenSlidePageFragment();
        Bundle args = new Bundle();
        args.putSerializable(DASHBOARD_KEY, dashboard);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mDashboard = (Dashboard) getArguments().getSerializable(DASHBOARD_KEY);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_main_screen_slide_page, container, false);
        initUI(mainView);
        populateDashboardDataOnUI();

        return mainView;
    }


    private void initUI(View mainView) {
        titleTextView = (MontserratBoldTextView) mainView.findViewById(R.id.titleTextView);
        subtitleTextView = (MontserratRegularTextView) mainView.findViewById(R.id.subtitleTextView);
        subtitle2TextView = (MontserratRegularTextView) mainView.findViewById(R.id.subtitle2TextView);

    }


    private void populateDashboardDataOnUI() {
        titleTextView.setText(mDashboard.getTitle());
        subtitleTextView.setText(mDashboard.getSubtitle());
        subtitle2TextView.setText(mDashboard.getSubtitle2());

    }

}
