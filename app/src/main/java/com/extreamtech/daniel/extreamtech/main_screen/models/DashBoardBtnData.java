package com.extreamtech.daniel.extreamtech.main_screen.models;

import com.extreamtech.daniel.extreamtech.R;

/**
 * Created by Daniel on 2016-09-14.
 */
public class DashBoardBtnData {

    private String title;
    private int imageResource;
    private int backgroundResource;


    public DashBoardBtnData(String title, int imageResource) {
        this.title = title;
        this.imageResource = imageResource;
        this.backgroundResource = R.color.dashboard_button_color_800;

    }

    public DashBoardBtnData(String title, int imageResource, int backgroundResource) {
        this.title = title;
        this.imageResource = imageResource;
        this.backgroundResource = backgroundResource;

    }


    public String getTitle() {
        return title;
    }

    public int getImageResource() {
        return imageResource;
    }

    public int getBackgroundResource() {
        return backgroundResource;
    }

}
