package com.extreamtech.daniel.extreamtech.crc_search;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import com.extreamtech.daniel.extreamtech.R;
import com.extreamtech.daniel.extreamtech.crc_search.models.Church;
import com.extreamtech.daniel.extreamtech.uicomponent.MontserratBoldTextView;
import com.extreamtech.daniel.extreamtech.uicomponent.MontserratRegularTextView;

import java.util.ArrayList;


public class ChurchListFragment extends SearchCRCBaseFragment {

    public static final int CHURCH_LIST_FRAGMENT_ID = 0;

    private SearchView searchView;
    private RecyclerView churchListView;
    private ChurchListViewAdapter churchListViewAdapter;

    public ChurchListFragment() {}


    public static ChurchListFragment newInstance() {
        ChurchListFragment fragment = new ChurchListFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_church_list, container, false);

        initUI(mainView);
        fetchChurchDataFromServer();

        return mainView;
    }


    private void fetchChurchDataFromServer() {
        mSelectCRCActivity.fetchChurchData(new FetchDataListener() {
            @Override
            public void dataFetchCallBack(ArrayList<Church> churchList) {
                churchListViewAdapter.setChurchList(churchList);
                churchListViewAdapter.notifyDataSetChanged();

            }
        }, false);

    }


    private void initUI(View mainView) {
        searchView = (SearchView) mainView.findViewById(R.id.searchView);
        churchListView = (RecyclerView) mainView.findViewById(R.id.churchListView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        churchListView.setLayoutManager(mLayoutManager);
        churchListViewAdapter = new ChurchListViewAdapter(getActivity(), mSelectCRCActivity.getChurchList());
        churchListView.setAdapter(churchListViewAdapter);
    }


    private class ChurchListViewAdapter extends RecyclerView.Adapter<ChurchViewHolders> {

        private ArrayList<Church> itemList;
        private Context mContext;

        public ChurchListViewAdapter(Context context, ArrayList<Church> itemList) {
            this.itemList = itemList;
            this.mContext = context;
        }


        @Override
        public ChurchViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(mContext).inflate(R.layout.church_list_view, parent, false);
            ChurchViewHolders rcv = new ChurchViewHolders(layoutView);
            return rcv;

        }


        public void setChurchList(ArrayList<Church> itemList) {
            this.itemList = itemList;
        }


        @Override
        public void onBindViewHolder(ChurchViewHolders holder, final int position) {
            holder.titleText.setText(itemList.get(position).getName());
            holder.subTitleText.setText(itemList.get(position).getAddress1());

            if ((position % 2) == 0) {
                holder.churchListViewMainLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.primary_text));

            } else {
                holder.churchListViewMainLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.divider_gray));

            }

            holder.churchListViewMainLayout.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    startChurchDetailActivity(itemList.get(position));

                }
            });

        }


        @Override
        public int getItemCount() {
            return this.itemList.size();

        }

    }


    public class ChurchViewHolders extends RecyclerView.ViewHolder {

        public MontserratBoldTextView titleText;
        public MontserratRegularTextView subTitleText;
        public RelativeLayout churchListViewMainLayout;

        public ChurchViewHolders(View itemView) {
            super(itemView);

            titleText = (MontserratBoldTextView) itemView.findViewById(R.id.titleText);
            subTitleText = (MontserratRegularTextView) itemView.findViewById(R.id.subTitleText);
            churchListViewMainLayout = (RelativeLayout) itemView.findViewById(R.id.church_list_view_main_layout);

        }

    }


}
