package com.extreamtech.daniel.extreamtech.uicomponent;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Daniel on 2016-09-14.
 * com.extreamtech.daniel.extreamtech.uicomponent.MontserratRegularTextView
 */
public class MontserratRegularTextView extends TextView {

    public MontserratRegularTextView(Context context) {
        super(context);
        init();
    }

    public MontserratRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MontserratRegularTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/montserrat_regular.ttf");
        setTypeface(tf);

    }

}
