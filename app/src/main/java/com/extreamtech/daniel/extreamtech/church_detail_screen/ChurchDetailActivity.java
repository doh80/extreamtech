package com.extreamtech.daniel.extreamtech.church_detail_screen;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.extreamtech.daniel.extreamtech.BaseActivity;
import com.extreamtech.daniel.extreamtech.R;
import com.extreamtech.daniel.extreamtech.crc_search.models.Church;
import com.extreamtech.daniel.extreamtech.crc_search.models.Contact;
import com.extreamtech.daniel.extreamtech.uicomponent.MontserratBoldTextView;
import com.extreamtech.daniel.extreamtech.uicomponent.MontserratRegularTextView;

import java.util.ArrayList;


public class ChurchDetailActivity extends BaseActivity {


    public static final String CHURCH_KEY = "CHURCH_KEY";
    public static final int CHURCH_SELECT_CODE = 234;
    public static final String CHURCH_SELECT_RESULT_CODE = "CHURCH_SELECT_RESULT_CODE";


    public Church church;

    private MontserratBoldTextView titleTextView;
    private MontserratRegularTextView addressTextView;
    private MontserratRegularTextView phoneNumTextView;
    private MontserratRegularTextView FaxNumTextView;
    private MontserratRegularTextView webAddrTextView;
    private MontserratRegularTextView emailAddrTextView;
    private MontserratRegularTextView worshipTimeTextView;
    private MontserratRegularTextView pastor_text;

    private Button selectButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_church_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.churchDetailToolBarView);
        toolbar.setTitle(getString(R.string.church_detail));
        setSupportActionBar(toolbar);

        initUI();

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                if(bundle.containsKey(CHURCH_KEY)) {
                    church = (Church) bundle.getSerializable(CHURCH_KEY);

                }
            }
        }

        populateData();

    }


    private void initUI() {
        titleTextView = (MontserratBoldTextView) findViewById(R.id.titleTextView);
        addressTextView = (MontserratRegularTextView) findViewById(R.id.addressTextView);
        phoneNumTextView = (MontserratRegularTextView) findViewById(R.id.phoneNumTextView);
        FaxNumTextView = (MontserratRegularTextView) findViewById(R.id.FaxNumTextView);
        webAddrTextView = (MontserratRegularTextView) findViewById(R.id.webAddrTextView);
        emailAddrTextView = (MontserratRegularTextView) findViewById(R.id.emailAddrTextView);
        worshipTimeTextView = (MontserratRegularTextView) findViewById(R.id.worshipTimeTextView);
        pastor_text = (MontserratRegularTextView) findViewById(R.id.pastor_text);

        selectButton = (Button) findViewById(R.id.selectButton);
        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnResult();

            }
        });

    }


    public void populateData() {
        titleTextView.setText(church.getName());
        addressTextView.setText(church.getAddress1());
        phoneNumTextView.setText(church.getPhone());
        FaxNumTextView.setText(church.getFax());
        webAddrTextView.setText(church.getWebsite());

        Contact contact = church.getFirstContactInfo();
        if (contact != null) {
            emailAddrTextView.setText(contact.getEmail());
        }

        ArrayList<String> serviceTimes = church.getServiceTimes();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < serviceTimes.size(); i++) {
            sb.append(serviceTimes.get(i));
            sb.append("\n");
        }
        worshipTimeTextView.setText(sb.toString());

//        pastor_text.setText(church.getP); // no pastor data found from server response

    }


    private void returnResult() {
        Intent intent = new Intent();
        intent.putExtra(CHURCH_SELECT_RESULT_CODE, church);
        setResult(RESULT_OK, intent);
        finish();

    }

}
