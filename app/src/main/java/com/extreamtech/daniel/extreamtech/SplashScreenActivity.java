package com.extreamtech.daniel.extreamtech;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.extreamtech.daniel.extreamtech.main_screen.MainActivity;

public class SplashScreenActivity extends AppCompatActivity {

    private int SLEEP_TIME = 2;

    private IntentLauncher launcher;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        this.launcher = new IntentLauncher();
        this.launcher.start();

    }

    private class IntentLauncher extends Thread {
        @Override
        public void run() {


            try {

                Thread.sleep(SLEEP_TIME*1000);
            } catch (Exception e) {
                Log.e("SplashScreenActivity", e.getMessage());
            }


            startMainActivity();
            SplashScreenActivity.this.finish();


        }

    }



    private void startMainActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        SplashScreenActivity.this.startActivity(intent);

    }


}
