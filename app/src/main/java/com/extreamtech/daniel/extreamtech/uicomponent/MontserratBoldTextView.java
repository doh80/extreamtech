package com.extreamtech.daniel.extreamtech.uicomponent;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Daniel on 2016-09-14.
 *
 * com.extreamtech.daniel.extreamtech.uicomponent.MontserratBoldTextView
 */
public class MontserratBoldTextView extends TextView {

    public MontserratBoldTextView(Context context) {
        super(context);
        init();
    }

    public MontserratBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MontserratBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/montserrat_bold.ttf");
        setTypeface(tf);

    }


}
