package com.extreamtech.daniel.extreamtech.crc_search.models;

import java.io.Serializable;

/**
 * Created by Daniel on 2016-09-15.
 */
public class Contact implements Serializable {

    private int id;
    private String name;
    private String title;
    private String phone;
    private String email;


    public Contact(int id, String name, String title, String phone, String email){
        this.id = id;
        this.name = name;
        this.title = title;
        this.phone = phone;
        this.email = email;

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTitle() {
        return title;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

}
