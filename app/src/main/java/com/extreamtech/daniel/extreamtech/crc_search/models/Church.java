package com.extreamtech.daniel.extreamtech.crc_search.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Daniel on 2016-09-15.
 */
public class Church implements Serializable {

    private int id;
    private String name;
    private String description;
    private String address1;
    private String address2;
    private String city;
    private String postalCode;
    private String province;
    private String latitude;
    private String longitude;
    private String phone;
    private String fax;
    private String website;

    private ArrayList<String> serviceTimes;
    private ArrayList<Contact> contacts;

    public Church() {}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getCity() {
        return city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getProvince() {
        return province;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getPhone() {
        return phone;
    }

    public String getFax() {
        return fax;
    }

    public String getWebsite() {
        return website;
    }

    public ArrayList<String> getServiceTimes() {
        return serviceTimes;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }


    public Contact getFirstContactInfo() {
        Contact contact = null;
        if (contacts != null && contacts.size() > 0) {
            contact = contacts.get(0);
        }

        return contact;
    }

}
