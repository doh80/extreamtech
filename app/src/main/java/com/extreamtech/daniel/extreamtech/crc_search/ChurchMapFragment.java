package com.extreamtech.daniel.extreamtech.crc_search;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.extreamtech.daniel.extreamtech.R;
import com.extreamtech.daniel.extreamtech.crc_search.models.Church;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;


public class ChurchMapFragment extends SearchCRCBaseFragment {

    private MapView mMapView;
    private GoogleMap googleMap;

    public static final int CHURCH_MAP_FRAGMENT_ID = 1;

    private HashMap<Marker, Church> churchHashMap = new HashMap<>();


    public ChurchMapFragment() {}

    public static ChurchMapFragment newInstance() {
        ChurchMapFragment fragment = new ChurchMapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    private void initUI(View mainView) {

    }

    private void initMap(View mainView, Bundle savedInstanceState) {

        mMapView = (MapView) mainView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                googleMap.setMyLocationEnabled(true);

                fetchChurchDataFromServer();

            }
        });

    }


    private void addMarkers() {
        mSelectCRCActivity.fetchChurchData(new FetchDataListener() {
            @Override
            public void dataFetchCallBack(ArrayList<Church> churchList) {
                for (int i = 0; i < churchList.size(); i++) {

                    String latitude = churchList.get(i).getLatitude();
                    String longitude = churchList.get(i).getLongitude();

                    if (!TextUtils.isEmpty(latitude) && !TextUtils.isEmpty(longitude)) {
                        LatLng position = new LatLng(Double.parseDouble(churchList.get(i).getLatitude()),
                                Double.parseDouble(churchList.get(i).getLongitude()));
                        MarkerOptions markerOptions = new MarkerOptions();
//                                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_place_white_24dp));
                        markerOptions.position(position).title(churchList.get(i).getName());
                        churchHashMap.put(googleMap.addMarker(markerOptions), churchList.get(i));

                        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                return false;
                            }
                        });


                        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

                            @Override
                            public void onInfoWindowClick(Marker marker) {
                                Church church = churchHashMap.get(marker);
                                startChurchDetailActivity(church);

                            }
                        });

                    }
                }
            }
        }, false);

        moveMapScreen();
    }


    private void moveMapScreen() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (Marker marker : churchHashMap.keySet()) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();

        int padding = 100; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        googleMap.moveCamera(cu);
        googleMap.animateCamera(cu);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_church_map, container, false);
//        initUI(mainView);
        initMap(mainView, savedInstanceState);

        return mainView;
    }


    private void fetchChurchDataFromServer() {
        mSelectCRCActivity.fetchChurchData(new FetchDataListener() {
            @Override
            public void dataFetchCallBack(ArrayList<Church> churchList) {
                addMarkers();

            }
        }, false);

    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onPause();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();

    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();

    }


    @Override
    public void onDetach() {
        super.onDetach();
        mMapView = null;

    }


}
