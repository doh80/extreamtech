package com.extreamtech.daniel.extreamtech.crc_search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.extreamtech.daniel.extreamtech.church_detail_screen.ChurchDetailActivity;
import com.extreamtech.daniel.extreamtech.crc_search.models.Church;

import java.util.ArrayList;


/**
 * Created by Daniel on 2016-09-15.
 */
public class SearchCRCBaseFragment extends Fragment {


    protected SelectCRCActivity mSelectCRCActivity;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    public boolean isFragmentActive() {
        return (getActivity() != null) && isAdded();

    }


    public interface FetchDataListener {
        void dataFetchCallBack(ArrayList<Church> churchList);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mSelectCRCActivity = (SelectCRCActivity) getActivity();

    }

    protected void startChurchDetailActivity(Church church) {
        Intent intent = new Intent((mSelectCRCActivity), ChurchDetailActivity.class);
        intent.putExtra(ChurchDetailActivity.CHURCH_KEY, church);
        mSelectCRCActivity.startActivityForResult(intent, ChurchDetailActivity.CHURCH_SELECT_CODE);

    }


}
