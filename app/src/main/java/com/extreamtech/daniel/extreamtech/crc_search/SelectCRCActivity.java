package com.extreamtech.daniel.extreamtech.crc_search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.extreamtech.daniel.extreamtech.BaseActivity;
import com.extreamtech.daniel.extreamtech.R;
import com.extreamtech.daniel.extreamtech.church_detail_screen.ChurchDetailActivity;
import com.extreamtech.daniel.extreamtech.crc_search.models.Church;
import com.extreamtech.daniel.extreamtech.engine.Engine;
import com.extreamtech.daniel.extreamtech.engine.VolleyManager;
import com.extreamtech.daniel.extreamtech.engine.VolleyResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;

public class SelectCRCActivity extends BaseActivity {

    private ArrayList<Church> churchList;

    private ChurchListFragment churchListFragment;
    private ChurchMapFragment churchMapFragment;
    private FragmentManager fragmentManager;

    private int currentFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_crc);

        initUI();

        this.churchListFragment = ChurchListFragment.newInstance();
        this.churchMapFragment = ChurchMapFragment.newInstance();

        this.fragmentManager = getSupportFragmentManager();
        this.fragmentManager.beginTransaction().replace(R.id.crc_serch_main_layout, churchListFragment).commit();
        this.currentFragment = ChurchListFragment.CHURCH_LIST_FRAGMENT_ID;

        this.churchList = new ArrayList<>();

    }


    private void initUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.select_crc_toolbar_view);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setTitle(getString(R.string.select_crc));
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;

    }


    private void switchFragment(Fragment fragment) {

        fragmentManager.beginTransaction().replace(R.id.crc_serch_main_layout, fragment).commit();

        if (fragment instanceof ChurchListFragment) {
            currentFragment = ChurchListFragment.CHURCH_LIST_FRAGMENT_ID;

        } else {
            currentFragment = ChurchMapFragment.CHURCH_MAP_FRAGMENT_ID;

        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_map) {
            if (currentFragment == ChurchListFragment.CHURCH_LIST_FRAGMENT_ID) {
                switchFragment(churchMapFragment);

            } else if (currentFragment == ChurchMapFragment.CHURCH_MAP_FRAGMENT_ID) {
                switchFragment(churchListFragment);

            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void fetchChurchDataFromServer(String request_url,
                                          Response.Listener responseListener,
                                          Response.ErrorListener ResponseErrorListener) {

        VolleyManager volleyManager = Engine.getInstance(this).getVolleyManager();
        volleyManager.getStringRequest(Request.Method.GET, request_url, new VolleyResponse<>(responseListener, ResponseErrorListener), null);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ChurchDetailActivity.CHURCH_SELECT_CODE) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();

            }
        }
    }


    public void fetchChurchData(final SearchCRCBaseFragment.FetchDataListener fetchDataListener, boolean isRefresh) {

        if (churchList != null && churchList.size() > 0 && (!isRefresh)) {
            fetchDataListener.dataFetchCallBack(churchList);
        }

        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Type collectionType = new TypeToken<Collection<Church>>(){}.getType();
                churchList = new Gson().fromJson(response, collectionType);
                fetchDataListener.dataFetchCallBack(churchList);

            }
        };

        Response.ErrorListener ResponseErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                fetchDataListener.dataFetchCallBack(churchList);

            }
        };


        fetchChurchDataFromServer(getResources().getString(R.string.church_url),
                responseListener, ResponseErrorListener);

    }


    public ArrayList<Church> getChurchList() {
        return churchList;
    }

}
