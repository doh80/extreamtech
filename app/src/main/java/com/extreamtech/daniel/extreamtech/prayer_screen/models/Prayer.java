package com.extreamtech.daniel.extreamtech.prayer_screen.models;


/**
 * Created by Daniel on 2016-09-14.
 */
public class Prayer {

    private String name;
    private String date;
    private String message;
    private int numberOfPrayers;


    public Prayer(String name, String date, String message, int numberOfPrayers) {
        this.name = name;
        this.date = date;
        this.message = message;
        this.numberOfPrayers = numberOfPrayers;
    }


    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public int getNumberOfPrayers() {
        return numberOfPrayers;
    }

}
