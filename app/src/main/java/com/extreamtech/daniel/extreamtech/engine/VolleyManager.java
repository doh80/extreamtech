package com.extreamtech.daniel.extreamtech.engine;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;


public class VolleyManager {

    private static VolleyManager mVolleyManager;
    private static RequestQueue mRequestQueue;
    private static Context mContext;


    private VolleyManager(Context ctx) {
        mContext = ctx;
        mRequestQueue = getRequestQueue();

    }


    public static VolleyManager getInstance(Context context) {
        if (mVolleyManager == null) {
            mVolleyManager = new VolleyManager(context);
        }
        return mVolleyManager;
    }


    public void shutdown() {
        if (mVolleyManager != null) {
            mRequestQueue = null;
            mContext = null;
            mVolleyManager = null;

        }
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext);
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);

    }


    public void getStringRequest(int requestMethod, String url, VolleyResponse<String> volleyResponse, String TAG) {

        StringRequest stringRequest = new StringRequest(requestMethod, url,
                volleyResponse.responseListener,
                volleyResponse.errorListener);

        if (TAG != null) {
            stringRequest.setTag(TAG);
        }

        mRequestQueue.add(stringRequest);
    }


    public void getJsonObjectRequest(int requestMethod, String url,
                                     VolleyResponse<JSONObject> volleyResponse, String TAG) {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(requestMethod, url,
                volleyResponse.responseListener,
                volleyResponse.errorListener);

        if (TAG != null) {
            jsonObjectRequest.setTag(TAG);
        }

        mRequestQueue.add(jsonObjectRequest);

    }


    public void cancelRequestByTag(String TAG) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }

    }


    public void cancelAllRequest() {
        mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });

    }


}