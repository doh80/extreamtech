package com.extreamtech.daniel.extreamtech.main_screen;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.extreamtech.daniel.extreamtech.R;
import com.extreamtech.daniel.extreamtech.church_detail_screen.ChurchDetailActivity;
import com.extreamtech.daniel.extreamtech.crc_search.SelectCRCActivity;
import com.extreamtech.daniel.extreamtech.crc_search.models.Church;
import com.extreamtech.daniel.extreamtech.engine.Engine;
import com.extreamtech.daniel.extreamtech.engine.VolleyResponse;
import com.extreamtech.daniel.extreamtech.main_screen.models.DashBoardBtnData;
import com.extreamtech.daniel.extreamtech.main_screen.models.Dashboard;
import com.extreamtech.daniel.extreamtech.prayer_screen.PrayersActivity;
import com.extreamtech.daniel.extreamtech.uicomponent.MontserratBoldTextView;
import com.extreamtech.daniel.extreamtech.uicomponent.MontserratRegularTextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class MainActivity extends AppCompatActivity {


    public static final String BRIDGE_PREFERENCES = "BRIDGE_PREFERENCES" ;
    public static final String CHURCH_ID_KEY = "CHURCH_ID_KEY";

    private MontserratRegularTextView midText;
    private MontserratRegularTextView mSelectedCrcText;
    private Toolbar mToolbar;
    private RecyclerView recycler_view;
    private RelativeLayout selectedCrcLayout;

    private SharedPreferences sharedpreferences;
    private ArrayList<DashBoardBtnData> dashBoardBtnList = new ArrayList<>();

    private ViewPager dashboardViewPager;
    private PagerAdapter mPagerAdapter;
    private ArrayList<Dashboard> dashboardList = new ArrayList<>();

    private Church mChurch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.sharedpreferences = getSharedPreferences(BRIDGE_PREFERENCES, Context.MODE_PRIVATE);

        initUI();
        retrieveChurchData();

    }


    @Override
    protected void onStop() {
        super.onStop();
        saveChurchData();
    }


    private void retrieveChurchData() {
        String restoredText = sharedpreferences.getString(CHURCH_ID_KEY, null);
        if (restoredText != null) {
            Gson gson = new Gson();
            mChurch = gson.fromJson(restoredText, Church.class);

            populateChurchData(mChurch);
            fetchDashBoardData();

        }
    }


    private void saveChurchData() {
        if (mChurch == null) { return; }

        SharedPreferences.Editor editor = sharedpreferences.edit();
        Gson gson = new Gson();
        editor.putString(CHURCH_ID_KEY, gson.toJson(mChurch));
        editor.commit();
    }


    private void initUI() {

        dashboardViewPager = (ViewPager) findViewById(R.id.dashboardViewPager);

        mToolbar = (Toolbar) findViewById(R.id.toolBarView);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
        mToolbar.setTitle(getString(R.string.the_bridge));

        midText = (MontserratRegularTextView) findViewById(R.id.midText);
        mSelectedCrcText = (MontserratRegularTextView) findViewById(R.id.selectedCrcText);

        selectedCrcLayout = (RelativeLayout) findViewById(R.id.selectedCrcLayout);
        selectedCrcLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SelectCRCActivity.class);
                startActivityForResult(intent, ChurchDetailActivity.CHURCH_SELECT_CODE);

            }
        });

        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        GridLayoutManager lLayout = new GridLayoutManager(MainActivity.this, 3);
        recycler_view.setLayoutManager(lLayout);

        initGridViewButtons();

    }


    private void initGridViewButtons() {

        dashBoardBtnList.add(new DashBoardBtnData(this.getResources().getString(R.string.watch), R.drawable.ic_play_circle_outline_white_24dp, R.color.dashboard_button_color_400));
        dashBoardBtnList.add(new DashBoardBtnData(this.getResources().getString(R.string.pray), R.drawable.ic_whatshot_white_24dp, R.color.dashboard_button_color_600));
        dashBoardBtnList.add(new DashBoardBtnData(this.getResources().getString(R.string.directory), R.drawable.ic_storage_white_24dp, R.color.dashboard_button_color_300));
        dashBoardBtnList.add(new DashBoardBtnData(this.getResources().getString(R.string.events), R.drawable.ic_insert_invitation_white_24dp, R.color.dashboard_button_color_500));
        dashBoardBtnList.add(new DashBoardBtnData(this.getResources().getString(R.string.give), R.drawable.ic_local_florist_white_24dp, R.color.dashboard_button_color_400));
        dashBoardBtnList.add(new DashBoardBtnData(this.getResources().getString(R.string.inspire), R.drawable.ic_color_lens_white_24dp, R.color.dashboard_button_color_700));

        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(this, dashBoardBtnList);
        recycler_view.setAdapter(recyclerViewAdapter);

    }


    private class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

        private List<DashBoardBtnData> itemList;
        private Context mContext;

        public RecyclerViewAdapter(Context context, List<DashBoardBtnData> itemList) {
            this.itemList = itemList;
            this.mContext = context;
        }

        @Override
        public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(mContext).inflate(R.layout.bashboard_view, parent, false);
            RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(RecyclerViewHolders holder, final int position) {
            holder.titleTextView.setText(itemList.get(position).getTitle());
            holder.iconImage.setImageResource(itemList.get(position).getImageResource());
            holder.dashboard_view_layout.setBackgroundColor(ContextCompat.getColor(mContext, itemList.get(position).getBackgroundResource()));
            holder.dashboard_view_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String title = itemList.get(position).getTitle();
                    if (title.equals(getResources().getString(R.string.pray))) {

                        if (mChurch != null) {
                            Intent intent = new Intent(MainActivity.this, PrayersActivity.class);
                            intent.putExtra(PrayersActivity.CHURCH_ID_KEY, mChurch.getId());
                            startActivity(intent);

                        } else {
                            Toast.makeText(mContext, mContext.getResources().getString(R.string.select_church_msg), Toast.LENGTH_SHORT).show();

                        }
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return this.itemList.size();
        }
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder {

        public MontserratBoldTextView titleTextView;
        public ImageView iconImage;
        public RelativeLayout dashboard_view_layout;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            titleTextView = (MontserratBoldTextView) itemView.findViewById(R.id.button_title);
            iconImage = (ImageView) itemView.findViewById(R.id.icon_photo);
            dashboard_view_layout = (RelativeLayout) itemView.findViewById(R.id.dashboard_view_layout);

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ChurchDetailActivity.CHURCH_SELECT_CODE) {
            if (resultCode == RESULT_OK) {
                mChurch = (Church) data.getExtras().getSerializable(ChurchDetailActivity.CHURCH_SELECT_RESULT_CODE);
                populateChurchData(mChurch);
                fetchDashBoardData();
            }
        }
    }


    private void populateChurchData(Church church) {
        midText.setVisibility(View.GONE);
        mSelectedCrcText.setVisibility(View.VISIBLE);
        mSelectedCrcText.setText(church.getName());

    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return MainScreenSlidePageFragment.newInstance(dashboardList.get(position));

        }

        @Override
        public int getCount() {
            return dashboardList.size();
        }
    }



    private void fetchDashBoardData() {

        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(getString(R.string.dash_board_url));
        urlBuilder.append(mChurch.getId());
        urlBuilder.append(getString(R.string.dash_board_url_end));

        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Type collectionType = new TypeToken<Collection<Dashboard>>(){}.getType();
                dashboardList.clear();
                dashboardList = new Gson().fromJson(response, collectionType);


                mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
                dashboardViewPager.setAdapter(mPagerAdapter);
            }
        };

        Response.ErrorListener ResponseErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        };

        Engine.getInstance(this).getVolleyManager().getStringRequest(Request.Method.GET,
                urlBuilder.toString(), new VolleyResponse<>(responseListener, ResponseErrorListener), null);

    }


}
